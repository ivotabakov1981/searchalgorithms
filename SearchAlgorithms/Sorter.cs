﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SearchAlgorithms
{
    class Sorter
    {
        public void Method1(List<int> input)
        {
            int counter = 0;

            for (int j = 0; j < input.Count; j++)
            {
                for (int i = 0; i < input.Count - 1-j; i++)
                {
                    counter++;

                    if (input[i] > input[i + 1])
                    {
                        int temp = input[i + 1];
                        input[i + 1] = input[i];
                        input[i] = temp;
                    }
                }
            }

            Console.WriteLine($"Iterations O(n): (n * (1 + n - 2)) / 2)");
            Console.WriteLine($"Iterations O(n): {(input.Count * (1 + input.Count - 2)) / 2}");
            Console.WriteLine($"Iterations O(n): {counter}");
           
        }

        internal List<int> Method2(List<int> input)
        {
            int counter = 0;
            List<int> res = new List<int>();

            res.Add(input[0]);

            for (int i = 1; i < input.Count; i++)
            {
                counter++;

                if (res[0] >= input[i])
                {
                    res.Insert(0, input[i]);                    
                }
                else
                {
                    int j = 0;
                    int curr = res[j];

                    while (input[i] > curr )
                    {
                        counter++;

                        if (j++ == res.Count)
                            break;

                        curr = res[j-1];                        
                    }

                    j--;

                    res.Insert(j, input[i]);
                }                                
            }

            //Console.WriteLine($"Iterations O(n): (n * (1 + n - 2)) / 2)");
            //Console.WriteLine($"Iterations O(n): {(input.Count * (1 + input.Count - 2)) / 2}");
            Console.WriteLine($"Iterations O(n): {counter}");

            return res;
        }
    }
}
