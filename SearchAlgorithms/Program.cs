﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SearchAlgorithms
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.BufferHeight = 1200;

            int n = 100;
            var numbers = Enumerable.Range(1, n).ToList();
            numbers.Shuffle();

            foreach (var number in numbers)
            {
                Console.WriteLine(number);
            }

            Console.WriteLine("");

            //numbers.Sort();
            //numbers = new List<int>(){ 2,1,7,6,9,8,4,5,10,3};

            Sorter s = new Sorter();
            //s.Method1(numbers);           
            numbers = s.Method2(numbers);

            foreach (var number in numbers)
            {
                Console.WriteLine(number);
            }

            Console.ReadLine();
        }
    }
}
